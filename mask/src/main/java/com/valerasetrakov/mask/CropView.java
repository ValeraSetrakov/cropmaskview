package com.valerasetrakov.mask;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.BindingAdapter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class CropView extends FrameLayout {

    private CropImageView cropImageView;

    private int cropImageViewColor;
    private Drawable cropImageViewDrawable;

    public CropView(Context context) {
        super(context);
        init(context);
    }

    public CropView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CropView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public CropView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init (Context context) {

        setClipChildren(false);
        this.cropImageView = new CropImageView(context);
        LayoutParams cropImageViewLayoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        cropImageViewLayoutParams.gravity = Gravity.CENTER;
        this.cropImageView.setAdjustViewBounds(true);
        this.cropImageView.setLayoutParams(cropImageViewLayoutParams);
        this.cropImageView.setColor(this.cropImageViewColor);
        this.cropImageView.setImageDrawable(this.cropImageViewDrawable);
        addView(this.cropImageView);

    }

    private void init (Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CropView);
        this.cropImageViewColor = typedArray.getColor(R.styleable.CropView_maskcolor, getThemeAccentColor(context));
        this.cropImageViewDrawable = typedArray.getDrawable(R.styleable.CropView_imageSrc);
        typedArray.recycle();
        init(context);
    }

    public static int getThemeAccentColor (final Context context) {
        final TypedValue value = new TypedValue ();
        context.getTheme ().resolveAttribute (R.attr.colorAccent, value, true);
        return value.data;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.cropImageView.setMaxWidth(w);
        this.cropImageView.setMaxHeight(h);
    }

    public void setImageSrc (String imageSrc) {
        GlideApp.with(this).load(imageSrc).into(this.cropImageView);
    }

    public void setImageSrc (int resourceId) {
        this.cropImageView.setImageResource(resourceId);
    }

    public void setImageSrc (Drawable drawable) {
        this.cropImageView.setImageDrawable(drawable);
    }

    public Path getCropBound () {
        return this.cropImageView.getCropBound();
    }

    public float getCropImageViewWidth () {
        return this.cropImageView.getWidth();
    }

    public float getCropImageViewHeight () {
        return this.cropImageView.getHeight();
    }


    @BindingAdapter("imageSrc")
    public static void setImageSrc (CropView view, String imageSrc) {
        view.setImageSrc(imageSrc);
    }

    @BindingAdapter("imageSrc")
    public static void setImageSrc (CropView view, int resourceId) {
        view.setImageSrc(resourceId);
    }

    @BindingAdapter("imageSrc")
    public static void setImageSrc (CropView view, Drawable drawable) {
        view.setImageSrc(drawable);
    }









    private class CropImageView extends AppCompatImageView {

        private static final int DEFAULT_INDEX_OF_MOVABLE_TOUCH_AREA = -1;

        private Paint boundPaint;
        private Paint touchAreaPaint;
        private int color;

        private static final int LEFT_TOP_TOUCH_AREA_POSITION = 0;
        private static final int RIGHT_TOP_TOUCH_AREA_POSITION = 1;
        private static final int RIGHT_BOTTOM_TOUCH_AREA_POSITION = 2;
        private static final int LEFT_BOTTOM_TOUCH_AREA_POSITION = 3;

        private static final int LEFT_MIDDLE_TOUCH_AREA_POSITION = 0;
        private static final int TOP_MIDDLE_TOUCH_AREA_POSITION = 1;
        private static final int RIGHT_MIDDLE_TOUCH_AREA_POSITION = 2;
        private static final int BOTTOM_MIDDLE_TOUCH_AREA_POSITION = 3;

        private RectF[] cornersTouchAreas = new RectF[4];
        private RectF[] middleTouchAreas = new RectF[4];
        private Path cropBound;

        private RectF checkableTouchArea = new RectF();

        private float roundX;
        private float roundY;

        private float touchAreaSize = 20;
        private float halfTouchAreaSize = touchAreaSize / 2;

        private int indexOfMovableCornerTouchArea = CropImageView.DEFAULT_INDEX_OF_MOVABLE_TOUCH_AREA;
        private int indexOfMovableMiddleTouchArea = CropImageView.DEFAULT_INDEX_OF_MOVABLE_TOUCH_AREA;

        private RectF firstRelativeTouchArea = null;
        private RectF secondRelativeTouchArea = null;

        public CropImageView(Context context) {
            super(context);
            init(context);
        }

        private void init (Context context) {
            this.cropBound = new Path();

            for (int i = 0; i < this.cornersTouchAreas.length; i++) {
                this.cornersTouchAreas[i] = new RectF();
                this.middleTouchAreas[i] = new RectF();
            }

            color = getThemeAccentColor(context);

            this.boundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            this.boundPaint.setStyle(Paint.Style.STROKE);
            this.boundPaint.setColor(color);
            this.boundPaint.setStrokeWidth(20);

            this.touchAreaPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            this.touchAreaPaint.setStyle(Paint.Style.FILL);
            this.touchAreaPaint.setColor(color);
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);

            int width = w;

            this.touchAreaSize = width / 10;
            this.halfTouchAreaSize = this.touchAreaSize / 2;
            this.roundX = this.halfTouchAreaSize;
            this.roundY = this.roundX;

            int left = 0;
            int top = 0;
            int right = left + w;
            int bottom = top + h;

            //corners

            RectF leftTopTouchArea = this.cornersTouchAreas[LEFT_TOP_TOUCH_AREA_POSITION];
            leftTopTouchArea.left = left - this.halfTouchAreaSize;
            leftTopTouchArea.top = top - this.halfTouchAreaSize;
            leftTopTouchArea.right = leftTopTouchArea.left + this.touchAreaSize;
            leftTopTouchArea.bottom = leftTopTouchArea.top + this.touchAreaSize;


            RectF rightTopTouchArea = this.cornersTouchAreas[RIGHT_TOP_TOUCH_AREA_POSITION];
            rightTopTouchArea.left = right - this.halfTouchAreaSize;
            rightTopTouchArea.top = top - this.halfTouchAreaSize;
            rightTopTouchArea.right = rightTopTouchArea.left + this.touchAreaSize;
            rightTopTouchArea.bottom = rightTopTouchArea.top + this.touchAreaSize;


            RectF rightBottomTouchArea = this.cornersTouchAreas[RIGHT_BOTTOM_TOUCH_AREA_POSITION];
            rightBottomTouchArea.left = right - this.halfTouchAreaSize;
            rightBottomTouchArea.top = bottom - this.halfTouchAreaSize;
            rightBottomTouchArea.right = rightBottomTouchArea.left + this.touchAreaSize;
            rightBottomTouchArea.bottom = rightBottomTouchArea.top + this.touchAreaSize;


            RectF leftBottomTouchArea = this.cornersTouchAreas[LEFT_BOTTOM_TOUCH_AREA_POSITION];
            leftBottomTouchArea.left = left - this.halfTouchAreaSize;
            leftBottomTouchArea.top = bottom - this.halfTouchAreaSize;
            leftBottomTouchArea.right = leftBottomTouchArea.left + this.touchAreaSize;
            leftBottomTouchArea.bottom = leftBottomTouchArea.top + this.touchAreaSize;

        }

        public void setColor (int color) {
            this.color = color;
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            Drawable currentDrawable = getDrawable();

            this.boundPaint.setColor(color);
            this.touchAreaPaint.setColor(color);

            if (currentDrawable != null) {
                drawCropMask(canvas);
            }
        }

        private void drawCropMask (Canvas canvas) {

            drawCropMaskCornersTouchAreas(canvas);
            drawCropMaskMiddlesTouchAreas(canvas);
            drawCropMaskBounds(canvas);


        }

        private void drawCropMaskCornersTouchAreas(Canvas canvas) {
            for (int i = 0; i < this.cornersTouchAreas.length; i++) {
                RectF cornerTouchArea = this.cornersTouchAreas[i];
                canvas.drawRoundRect(cornerTouchArea, this.roundX, this.roundY, this.touchAreaPaint);

            }
        }

        private void drawCropMaskMiddlesTouchAreas(Canvas canvas) {

            drawCropMaskMiddleTouchArea(canvas, LEFT_MIDDLE_TOUCH_AREA_POSITION, LEFT_TOP_TOUCH_AREA_POSITION, LEFT_BOTTOM_TOUCH_AREA_POSITION);
            drawCropMaskMiddleTouchArea(canvas, TOP_MIDDLE_TOUCH_AREA_POSITION, LEFT_TOP_TOUCH_AREA_POSITION, RIGHT_TOP_TOUCH_AREA_POSITION);
            drawCropMaskMiddleTouchArea(canvas, RIGHT_MIDDLE_TOUCH_AREA_POSITION, RIGHT_TOP_TOUCH_AREA_POSITION, RIGHT_BOTTOM_TOUCH_AREA_POSITION);
            drawCropMaskMiddleTouchArea(canvas, BOTTOM_MIDDLE_TOUCH_AREA_POSITION, RIGHT_BOTTOM_TOUCH_AREA_POSITION, LEFT_BOTTOM_TOUCH_AREA_POSITION);


        }

        private void drawCropMaskMiddleTouchArea(Canvas canvas, int indexMiddleTouchArea, int indexFirstCornerTouchArea, int indexSecondCornerTouchArea) {

            RectF middleTouchArea = this.middleTouchAreas[indexMiddleTouchArea];
            RectF firstCornerTouchArea = this.cornersTouchAreas[indexFirstCornerTouchArea];
            RectF secondCornerTouchArea = this.cornersTouchAreas[indexSecondCornerTouchArea];

            middleTouchArea.left = firstCornerTouchArea.left + ((secondCornerTouchArea.left - firstCornerTouchArea.left) / 2);
            middleTouchArea.top = firstCornerTouchArea.top + ((secondCornerTouchArea.top - firstCornerTouchArea.top) / 2);
            middleTouchArea.right = middleTouchArea.left + this.touchAreaSize;
            middleTouchArea.bottom = middleTouchArea.top + this.touchAreaSize;

            canvas.drawRoundRect(middleTouchArea, this.roundX, this.roundY, this.touchAreaPaint);


        }

        private void drawCropMaskBounds (Canvas canvas) {

            Path bound = this.cropBound;
            bound.reset();

            int cornersTouchAreasLength = this.cornersTouchAreas.length;

            for (int i = 0; i < cornersTouchAreasLength; i++) {

                RectF cornerTouchArea = this.cornersTouchAreas[i];

                if (i == 0) {
                    float x = cornerTouchArea.centerX();
                    float y = cornerTouchArea.centerY();
                    bound.moveTo(x, y);
                } else {
                    float x = cornerTouchArea.centerX();
                    float y = cornerTouchArea.centerY();
                    bound.lineTo(x, y);
                }

                if (i == cornersTouchAreasLength - 1){
                    bound.close();
                }
            }

            canvas.drawPath(bound, boundPaint);

        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            return super.onTouchEvent(event) || touchHandle(event);
        }

        private boolean touchHandle (MotionEvent event) {

            int actionType = event.getAction();
            float eventX = event.getX();
            float eventY = event.getY();

            switch (actionType) {
                case MotionEvent.ACTION_DOWN: {

                    //проверяем что мы жмем на угол
                    this.indexOfMovableCornerTouchArea = getIndexCornerTouchAreaByCoordinates(eventX, eventY);

                    //если не на угол, то проверяем что жмем на центр ребра
                    if (this.indexOfMovableCornerTouchArea == CropImageView.DEFAULT_INDEX_OF_MOVABLE_TOUCH_AREA)
                        this.indexOfMovableMiddleTouchArea = getIndexMiddleTouchAreaByCoordinates(eventX, eventY);

                    if (this.indexOfMovableMiddleTouchArea != CropImageView.DEFAULT_INDEX_OF_MOVABLE_TOUCH_AREA) {
                        determineRelativeCornerTouchAreaByMiddleTouchAreaIndex(this.indexOfMovableMiddleTouchArea);
                    }

                    return this.indexOfMovableCornerTouchArea != CropImageView.DEFAULT_INDEX_OF_MOVABLE_TOUCH_AREA ||
                            this.indexOfMovableMiddleTouchArea != CropImageView.DEFAULT_INDEX_OF_MOVABLE_TOUCH_AREA;

                }
                case MotionEvent.ACTION_MOVE: {

                    if (this.indexOfMovableCornerTouchArea != CropImageView.DEFAULT_INDEX_OF_MOVABLE_TOUCH_AREA) {
                        moveCornerTouchArea(this.indexOfMovableCornerTouchArea, eventX, eventY);
                    } else if (this.indexOfMovableMiddleTouchArea != CropImageView.DEFAULT_INDEX_OF_MOVABLE_TOUCH_AREA) {
                        moveMiddleTouchArea(this.indexOfMovableMiddleTouchArea, eventX, eventY);
                    } else {
                        return true;
                    }
                    break;

                }
                case MotionEvent.ACTION_UP: {
                    this.indexOfMovableCornerTouchArea = CropImageView.DEFAULT_INDEX_OF_MOVABLE_TOUCH_AREA;
                    this.indexOfMovableMiddleTouchArea = CropImageView.DEFAULT_INDEX_OF_MOVABLE_TOUCH_AREA;
                    this.firstRelativeTouchArea = null;
                    this.secondRelativeTouchArea = null;
                    break;
                }
            }

            return false;
        }

        private boolean prepareCornerTouchArea (int indexOfMovableCornerTouchArea, float x, float y) {
            RectF movableTouchArea = this.cornersTouchAreas[indexOfMovableCornerTouchArea];
            return prepareCornerTouchArea(movableTouchArea, x, y);
        }

        private boolean prepareCornerTouchArea (RectF movableTouchArea, float x, float y) {
            this.checkableTouchArea.set(movableTouchArea);
            this.checkableTouchArea.offsetTo(x, y);

            float minX = 0;
            float minY = 0;
            float maxX = getWidth();
            float maxY = getHeight();

            x = Math.min(maxX - this.halfTouchAreaSize, Math.max(minX - this.halfTouchAreaSize, x));
            y = Math.min(maxY - this.halfTouchAreaSize, Math.max(minY - this.halfTouchAreaSize, y));

            //TODO need check intersects between two diff touch area, code bellow doesn't work, need fix it!

//        for (int i = 0; i < this.cornersTouchAreas.length; i++) {
//
//            if (i == indexOfMovableCornerTouchArea)
//                continue;
//
//            RectF cornerTouchArea = this.cornersTouchAreas[i];
//
//            if (RectF.intersects(cornerTouchArea, this.checkableTouchArea)) {
//                Log.d(TAG, "prepareCornerTouchArea: return false");
//                return false;
//            }
//
//        }

            movableTouchArea.offsetTo(x, y);

            return true;
        }

        private void moveCornerTouchArea (int indexOfMovableCornerTouchArea, float x, float y) {
            if (prepareCornerTouchArea(indexOfMovableCornerTouchArea, x, y))
                invalidate();
        }

        private void moveMiddleTouchArea (int indexOfMovableTouchArea, float x, float y) {

            if (firstRelativeTouchArea == null || secondRelativeTouchArea == null) {
                return;
            }

            RectF movableTouchArea = this.middleTouchAreas[indexOfMovableTouchArea];
            float movableTouchAreaCenterX = movableTouchArea.centerX();
            float movableTouchAreaCenterY = movableTouchArea.centerY();

            float a = x - movableTouchAreaCenterX;
            float b = y - movableTouchAreaCenterY;
            float firstNewX = firstRelativeTouchArea.centerX() + a;
            float firstNewY = firstRelativeTouchArea.centerY() + b;

            float secondNewX = secondRelativeTouchArea.centerX() + a;
            float secondNewY = secondRelativeTouchArea.centerY() + b;

            prepareCornerTouchArea(firstRelativeTouchArea, firstNewX, firstNewY);
            prepareCornerTouchArea(secondRelativeTouchArea, secondNewX, secondNewY);
            invalidate();
        }

        private void determineRelativeCornerTouchAreaByMiddleTouchAreaIndex (int indexOfMovableMiddleTouchArea) {
            switch (indexOfMovableMiddleTouchArea) {
                case LEFT_MIDDLE_TOUCH_AREA_POSITION: {
                    this.firstRelativeTouchArea = this.cornersTouchAreas[LEFT_TOP_TOUCH_AREA_POSITION];
                    this.secondRelativeTouchArea = this.cornersTouchAreas[LEFT_BOTTOM_TOUCH_AREA_POSITION];
                    break;
                }
                case TOP_MIDDLE_TOUCH_AREA_POSITION: {
                    this.firstRelativeTouchArea = this.cornersTouchAreas[LEFT_TOP_TOUCH_AREA_POSITION];
                    this.secondRelativeTouchArea = this.cornersTouchAreas[RIGHT_TOP_TOUCH_AREA_POSITION];
                    break;
                }
                case RIGHT_MIDDLE_TOUCH_AREA_POSITION: {
                    this.firstRelativeTouchArea = this.cornersTouchAreas[RIGHT_TOP_TOUCH_AREA_POSITION];
                    this.secondRelativeTouchArea = this.cornersTouchAreas[RIGHT_BOTTOM_TOUCH_AREA_POSITION];
                    break;
                }
                case BOTTOM_MIDDLE_TOUCH_AREA_POSITION: {
                    this.firstRelativeTouchArea = this.cornersTouchAreas[RIGHT_BOTTOM_TOUCH_AREA_POSITION];
                    this.secondRelativeTouchArea = this.cornersTouchAreas[LEFT_BOTTOM_TOUCH_AREA_POSITION];
                    break;
                }
            }
        }

        /**
         * @return index of touchAreas array if x and y contains on of touch areas array elements or -1
         */
        private int getIndexCornerTouchAreaByCoordinates(float x, float y) {

            for (int i = 0; i < cornersTouchAreas.length; i++) {
                RectF touchArea = cornersTouchAreas[i];
                if (touchArea.contains(x, y))
                    return i;
            }

            return -1;
        }

        private int getIndexMiddleTouchAreaByCoordinates(float x, float y) {

            for (int i = 0; i < middleTouchAreas.length; i++) {
                RectF touchArea = middleTouchAreas[i];
                if (touchArea.contains(x, y))
                    return i;
            }

            return -1;
        }

        private boolean checkTouchCoordinateForMovingCropMask(int indexOfMovableTouchArea, float x, float y) {

            RectF movableTouchArea = this.cornersTouchAreas[indexOfMovableTouchArea];
            this.checkableTouchArea.set(movableTouchArea);
            this.checkableTouchArea.offsetTo(x, y);

            float minX = 0;
            float minY = 0;
            float maxX = getWidth();
            float maxY = getHeight();

            float newX = this.checkableTouchArea.centerX();
            float newY = this.checkableTouchArea.centerY();

            boolean isIntersectBounds = newX <= minX ||
                    newY <= minY ||
                    newX >= maxX ||
                    newY >= maxY;

            if (isIntersectBounds)
                return false;

            for (int i = 0; i < this.cornersTouchAreas.length; i++) {

                if (indexOfMovableTouchArea == i)
                    continue;

                RectF touchArea = this.cornersTouchAreas[i];
                if (this.checkableTouchArea.intersect(touchArea))
                    return false;
            }

            return true;
        }

        public Path getCropBound () {
            return this.cropBound;
        }

    }
}
